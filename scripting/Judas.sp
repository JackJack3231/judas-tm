#pragma semicolon 1

#define DEBUG

#define PLUGIN_AUTHOR "JackJack3231™"
#define PLUGIN_VERSION "0.17"

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <string>
//#include <sdkhooks>

EngineVersion g_Game;

public Plugin:myinfo = 
{
	name = "Judas™",
	author = PLUGIN_AUTHOR,
	description = "Renaming Denis to Judas",
	version = PLUGIN_VERSION,
	url = ""
};

public OnPluginStart()
{
	g_Game = GetEngineVersion();
	if(g_Game != Engine_CSGO && g_Game != Engine_CSS)
	{
		SetFailState("This plugin is for CSGO/CSS only.");	
	}
	
	HookEvent("player_activate", Event_PlayerActive);
	HookEvent("player_changename", Event_PlayerChangename);
	
	RegAdminCmd("judas_help", Judas_Help, ADMFLAG_GENERIC);
	RegAdminCmd("judas_version", Judas_Version, ADMFLAG_GENERIC);
	RegAdminCmd("judas_watching", Judas_Watch, ADMFLAG_CHAT);
	RegAdminCmd("judas_connect", Judas_Connect, ADMFLAG_CHAT);
	RegAdminCmd("judas_colors", Judas_Colors, ADMFLAG_GENERIC);
	RegAdminCmd("judas_countdown", Judas_CountDown, ADMFLAG_CHAT);
	RegAdminCmd("judas_kick", Judas_Kick, ADMFLAG_KICK);
	RegAdminCmd("judas_mute", Judas_Mute, ADMFLAG_ROOT);
	RegAdminCmd("judas_status", Judas_Status, ADMFLAG_ROOT);

}
//CHAT COLOR CODES
// x01 #ffffff		x06 #a2ff47		x0B #5e98d9
// x02 #ff0000		x07 #ff4040		x0C #4b69ff
// x03 #ba81f0		x08 #c5cad0		x0D #b0c3d9
// x04 #40ff40		x09 #ede47a		x0E #d32ce6
// x05 #bfff90		x0A #b0c3d9		x0F #eb4b4b

//GLOBAL VARS------------------------------------------------------------------------------------------------
char ChatPrefix[] = "[\x05Judas™\x01]";
char HintPrefix[] = "<font color='#ffffff'>[<font color='#bfff90'>Judas™<font color='#ffffff'>]";
bool JudasMute = false;

//COMMANDS---------------------------------------------------------------------------------------------------
public Action Judas_Help(int client, int args)
{
	PrintToChat(client, "%s \x0AJudas™ Commands:", ChatPrefix);
	PrintToChat(client, "%s \x04judas_help\x01  Display Judas™ Commands", ChatPrefix);
	PrintToChat(client, "%s \x04judas_version\x01  Display Version of Judas™", ChatPrefix);
	PrintToChat(client, "%s \x04judas_watching\x01  Display Always Watching Hint", ChatPrefix);
	PrintToChat(client, "%s \x04judas_connect \x09X\x01 Simmulate Connect of User \x09X", ChatPrefix);
	PrintToChat(client, "%s \x04judas_colors\x01  Display ChatColorCodes", ChatPrefix);
	PrintToChat(client, "%s \x04judas_countdown \x09X\x01 Display \x09X \x01Second CountDown", ChatPrefix);
	PrintToChat(client, "%s \x04judas_kick \x09X\x01 Kick all Players after \x09X \x01Seconds", ChatPrefix);
	return Plugin_Handled;
}

public Action Judas_Version(int client, int args)
{
	PrintToChat(client, "%s \x0AJudas™ working \x01(Version \x09%s\01)", ChatPrefix, PLUGIN_VERSION);
	return Plugin_Handled;
}

public Action Judas_Watch(int client, int args)
{
	PrintHintTextToAll("%s <font color='#ff0000'>We're always watching</font>", HintPrefix);	
	return Plugin_Handled;
}

public Action Judas_Connect(int client, int args)
{
	char who[32];
	GetCmdArg(1, who, sizeof(who));
	ConnectMessage(who);
	return Plugin_Handled;
}

public Action Judas_Colors(int client, int args)
{
	PrintToChat(client, "%s \x011\xFF\x022\xFF\x033\xFF\x044\xFF\x055\xFF\x066\xFF\x077\xFF\x088\xFF\x099\xFF\x0AA\xFF\x0BB\xFF\x0CC\xFF\x0DD\xFF\x0EE\xFF\x0FF\xFF", ChatPrefix);
	return Plugin_Handled;
}

public Action Judas_CountDown(int client, int args)
{
	if (args < 1)
	{
		PrintToConsole(client, "not enough Args");
		return Plugin_Handled;
	}
	char arg1[3];
	GetCmdArg(1, arg1, sizeof(arg1));
	int Length = StringToInt(arg1, 10);
	if (Length == 0)
	{
		PrintToConsole(client, "cannot be 0");
		return Plugin_Handled;
	}
	CountDown(Length);
	return Plugin_Handled;
}

public Action Judas_Kick(int client, int args)
{
	if (args < 1)
	{
		PrintToConsole(client, "not enough Args");
		return Plugin_Handled;
	}
	char arg1[3];
	GetCmdArg(1, arg1, sizeof(arg1));
	int Length = StringToInt(arg1, 10);
	if (Length == 0)
	{
		PrintToConsole(client, "cannot be 0");
		return Plugin_Handled;
	}
	Kick_CountDown(Length);
	return Plugin_Handled;
}

public Action Judas_Mute(int client, int args)
{
	if (JudasMute == false)
	{
		JudasMute = true;
		PrintToChat(client, "%s Judas™ is now Muted", ChatPrefix);
	}
	else
	{
		JudasMute = false;
		PrintToChat(client, "%s Judas™ is now UnMuted", ChatPrefix);
	}
	return Plugin_Handled;
}

public Action Judas_Status(int client, int args)
{
	if (JudasMute == false)
	{
		PrintToChat(client, "%s Judas™ NotMuted", ChatPrefix);
	}
	else
	{
		PrintToChat(client, "%s Judas™ Muted", ChatPrefix);
	}
	return Plugin_Handled;
}
//ACTIONS----------------------------------------------------------------------------------------------------
public Action Kick_End(Handle Timer)
{
	ServerCommand("sm_kick @humans");
	//PrintToChatAll("OVER");
}

public Action CountDown_Output(Handle Timer, int Number)
{
	PrintToChatAll("%s %i", ChatPrefix, Number);
}

public Action CountDown_End(Handle Timer)
{
	PrintToChatAll("%s Countdown over", ChatPrefix);
}
//FUNCTIONS--------------------------------------------------------------------------------------------------
public void ConnectMessage(char[] who)
{
	if (JudasMute == true)
	{
		return;
	}
	PrintToChatAll("%s \x02%s\x01 Connected", ChatPrefix, who);
	PrintHintTextToAll("%s <font color='#ff0000'>%s<font color='#ffffff'> Connected</font>", HintPrefix, who);
}

public void JudasRename(int UserID, char[] Name)
{
	ServerCommand("sm_rename #%i %s", UserID, Name);
}

public void Kick_CountDown(int Length)
{
	float NewLength = 0.0 + Length;
	
	PrintToChatAll("%s Kicking all Players in:", ChatPrefix);
	CreateTimer(NewLength, Kick_End);
	CreateTimer(0.0, CountDown_Output, Length);
	
	for (new i = Length - 1; i > 0; i--)
	{
		float NewI = 0.0 + i;
		int Number = Length - i;
		CreateTimer(NewI, CountDown_Output, Number);
	}
}

public void CountDown(int Length)
{
	float NewLength = 0.0 + Length;
	CreateTimer(NewLength, CountDown_End);
	CreateTimer(0.0, CountDown_Output, Length);
	
	for (new i = Length - 1; i > 0; i--)
	{
		float NewI = 0.0 + i;
		int Number = Length - i;
		CreateTimer(NewI, CountDown_Output, Number);
	}
}

//EVENTS-----------------------------------------------------------------------------------------------------
public void Event_PlayerActive(Event event, const char[] name, bool dontBroadcast)
{
	int UserID = event.GetInt("userid");
	int ClientID = GetClientOfUserId(UserID);
	
	if (IsFakeClient(ClientID) == false)
	{
		int SteamID = GetSteamAccountID(ClientID);
		
		if (SteamID == 117670946) //DenisPUMP
		{
			JudasRename(UserID, "Judas");
		}
			/*
			(SteamID == 68937047) //JackJack3231
			(SteamID == 314790480) //Teepster
			(SteamID == 187356933) //TwinZi
			(SteamID == 94556109) //Nexiz
			(SteamID == 180719388) //IronTony
			*/
	
		char who[32];
		GetClientName(ClientID, who, sizeof(who));
		ConnectMessage(who);
	}
}

public void Event_PlayerChangename(Event event, const char[] name, bool dontBroadcast)
{	
	int UserID = event.GetInt("userid");
	int ClientID = GetClientOfUserId(UserID);
	
	if (IsFakeClient(ClientID) == false)
	{
		int SteamID = GetSteamAccountID(ClientID);
	
		if (SteamID == 117670946)
		{
			new String:NewName[32];
			event.GetString("newname", NewName, sizeof(NewName));
		
			if (StrEqual(NewName, "Judas") == false)
			{
				JudasRename(UserID, "Judas");
			}
		}
		/*if (SteamID == 68937047)
		{
			if ((StrEqual(NewName, "OberGott", true)) == false)
			{
				//event.SetString("newname", "OberGott");
				//SetClientName(ClientID, "OberGott");
				ServerCommand("sm_rename #%i OberGott", UserID);
			}	
		}*/
	}
}